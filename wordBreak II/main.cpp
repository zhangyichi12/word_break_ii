//
//  main.cpp
//  wordBreak II
//
//  Created by Yichi Zhang on 10/12/14.
//  Copyright (c) 2014 Yichi Zhang. All rights reserved.
//

#include <iostream>
#include <vector>
#include <unordered_set>
using namespace std;

void helper(const string &s, unordered_set<string> &dict, vector<string> &result, string &path, int pos, const int &maxLength, vector<bool> &canBreak){
    if(pos == s.size()){
        path.erase(path.end() - 1);
        result.push_back(path);
    }
    
    for(int i = pos; i <= s.size(); i++){
        for(int wordLength = 0; wordLength < maxLength; wordLength++){
            if(i + wordLength >= s.size()){
                continue;
            }
            
            unordered_set<string>::iterator it;
            string subString = s.substr(i, wordLength + 1);
            it = dict.find(subString);
            

            if(it != dict.end() && canBreak[i] == 1){
                canBreak[i + wordLength + 1] = 1;
                string::iterator pathEnd = path.end();
                path.insert(pathEnd, subString.begin(), subString.end());
                path.append(" ");
                helper(s, dict, result, path, i + wordLength + 1, maxLength, canBreak);
                path.erase(pathEnd, path.end());
                canBreak[i + wordLength + 1] = 0;
            }
        }
    }
}

vector<string> wordBreak(string s, unordered_set<string> &dict) {
    
    vector<string> result;
    string path;
    
    unordered_set<string>::iterator it = dict.begin();
    int maxLength = 0;
    int temp = 0;
    while(it != dict.end()){
        temp = it->size();
        if(temp > maxLength){
            maxLength = temp;
        }
        it++;
    }
    
    vector<bool> canBreak(s.size() + 1, 0);
    canBreak[0] = 1;
    helper(s, dict, result, path, 0, maxLength, canBreak);
    return result;
}




int main(int argc, const char * argv[]) {
   
    string s = "catsanddog";
    unordered_set<string> dict;
    dict.insert("cat");
    dict.insert("cats");
    dict.insert("and");
    dict.insert("sand");
    dict.insert("dog");
    dict.insert("d");
    dict.insert("og");

    /*
    string s = "a";
    unordered_set<string> dict;
    dict.insert("a");
    */
    
    vector<string> vs(wordBreak(s, dict));
    
    for(int i = 0; i < vs.size(); i++){
        cout << vs[i] <<endl;
    }
    
    return 0;
}






